package gException;

import gView.Config;

/**
 * Exception for the maximal number of vertices.
 * 
 * @version 1.0 04.12.2014
 * @author Hinderlich
 * @author Ludwig
 *
 */
public class MaximalVerticesException extends Exception {

	/**
	 * default UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default message for this Exception.
	 */
	private static final String msg = "Die Maximalanzahl ("
			+ Config.MAXIMAL_VERTICES + ") von Knoten ist erreicht.";

	/**
	 * Constructor of Exception.
	 */
	public MaximalVerticesException() {
		super(msg);
	}

}
