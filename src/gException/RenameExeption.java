package gException;

/**
 * Exception if the Renaming of vertex fails.
 * 
 * @version 1.0 04.12.2014
 * @author Hinderlich
 * @author Ludwig
 *
 */
public class RenameExeption extends Exception {

	/**
	 * default UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * Constructor of Exception.
	 *
	 * @param msg
	 *            message of the exception
	 */
	public RenameExeption(String msg) {
		super(msg);
	}
}
