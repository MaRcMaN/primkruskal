package gException;

/**
 * Exception if the PreConditions of an minimal spanning tree are not given.
 * 
 * @version 1.0 04.12.2014
 * @author Hinderlich
 * @author Ludwig
 *
 */
public class PreConditionsException extends Exception {

	/**
	 * Default UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *
	 * Constructor of Exception.
	 *
	 * @param msg
	 *            message of the exception
	 */
	public PreConditionsException(String msg) {
		super(msg);
	}

}
