package gException;

/**
 * Exception for the creation of new edges.
 * 
 * @version 1.0 04.12.2014
 * @author Hinderlich
 * @author Ludwig
 *
 */
public class EdgeCreateException extends Exception {

	/**
	 * Default UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor of Exception.
	 * 
	 * @param msg
	 *            message of the exception
	 */
	public EdgeCreateException(String msg) {
		super(msg);
	}

}
