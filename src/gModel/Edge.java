package gModel;

import java.io.Serializable;

/**
 * Edge model class.
 * 
 * implements Serializable for save and restore in a graph
 * 
 * implements Comparable for sorting of edges in a list
 * 
 * @version 1.0 04.12.2014
 * @author Hinderlich
 * @author Ludwig
 *
 */
public class Edge implements Serializable, Comparable<Edge> {

	private static final long serialVersionUID = 1L;
	Vertex from;
	Vertex to;
	int weight;
	boolean marker;
	int colour;
	Graph g;

	/**
	 * Constructor of edge.
	 * 
	 * @param left
	 *            Vertex to the left side of the edge
	 * @param right
	 *            Vertex to the righ side of the edge
	 * @param weight
	 *            integer value of the edge's weight
	 * @param g
	 *            Graph on which the edge should be painted
	 */
	public Edge(Vertex left, Vertex right, int weight, Graph g) {
		super();
		this.from = left;
		this.to = right;
		this.weight = weight;
		this.g = g;
		this.colour = 0;
		marker = false;
	}

	/**
	 * Constructor of edge.
	 * 
	 * @param left
	 *            Vertex to the left side of the edge
	 * @param right
	 *            Vertex to the righ side of the edge
	 * @param g
	 *            Graph
	 */
	public Edge(Vertex left, Vertex right, Graph g) {
		super();
		this.from = left;
		this.to = right;
		this.weight = 1;
		this.g = g;
		this.colour = 0;
		marker = false;
	}

	// getter & setter

	public int getColour() {
		return colour;
	}

	public void setColour(int colour) {
		this.colour = colour;
	}

	public Vertex getfrom() {
		return from;
	}

	public void setfrom(Vertex left) {
		this.from = left;
	}

	public Vertex getTo() {
		return to;
	}

	public void setTo(Vertex right) {
		this.to = right;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
		g.changed();
	}

	protected boolean meets(Vertex v) {
		return (from == v || to == v);
	}

	public void resetNonPersistentProps() {
		marker = false;
		colour = 0;
	}

	public boolean isMarker() {
		return marker;
	}

	public void setMarker(boolean marker) {
		this.marker = marker;
		g.changed();
	}

	public String toString() {
		return this.from + " nach " + this.to + " (" + this.weight + ")";
	}

	/**
	 * Method for comparing two edges when using the Collections sort Method
	 * returns compared Edge in ascending order note: for descending order
	 * return (compareWeight - this.weight)
	 */
	@Override
	public int compareTo(Edge compareEdge) {
		int compareWeight = compareEdge.getWeight();
		// ascending order
		return this.weight - compareWeight;
	}

}
