package gModel;

import gException.MaximalVerticesException;
import gException.PreConditionsException;
import gView.Config;
import gView.GFrame;
import gView.View;

import java.awt.Dimension;
import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * Graph model class.
 * 
 * implements Serializable for save and restore graph
 * 
 * extends Observable, observer pattern as observable class
 * 
 * @version 1.0 04.12.2014
 * @author Hinderlich
 * @author Ludwig
 *
 */
public class Graph extends Observable implements Serializable {

	/**
	 * Default UID.
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Set of all vertices.
	 */
	private HashSet<Vertex> vertices;
	/**
	 * Set of all edges.
	 */
	private HashSet<Edge> edges;
	/**
	 * Adjacency list.
	 */
	private AdjacencyList adjacencyList;
	/**
	 * 
	 */
	private int actualVertexNumber;
	/**
	 * Flag to the graph to be changed for re-rendering the view.
	 */
	private boolean changed;

	/**
	 * text area of the scrollpanel for the user log message at the end of mst
	 * calculation
	 */
	private JTextArea textArea;

	/**
	 * scrollpanel for the user log message at the end of mst calculation
	 */
	private JScrollPane scrollPane;

	/**
	 * Constructor of graph.
	 * 
	 * initializes vertices, edges and adjacencylist, actualVertexNumber and
	 * changed to default values
	 */
	public Graph() {
		super();
		this.vertices = new HashSet<Vertex>();
		this.edges = new HashSet<Edge>();
		this.adjacencyList = new AdjacencyList();
		actualVertexNumber = 0;
		changed = false;

		textArea = new JTextArea("Insert your Text here");
		scrollPane = new JScrollPane(textArea);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		scrollPane.setPreferredSize(new Dimension(550, 500));

	}

	/**
	 * Method for adding one vertex to the vertices list in the graph.
	 * 
	 * @param location
	 *            Point with x and y coordinates
	 * @return Vertex which is added to the vertices list
	 * @throws MaximalVerticesException
	 *             if the maximum value of vertices is reached
	 * 
	 *             Value is set as MAXIMAL_VERTICES in {@link Config}
	 */
	public Vertex addVertex(Point location) throws MaximalVerticesException {

		if (vertices.size() >= Config.MAXIMAL_VERTICES) {
			throw new MaximalVerticesException();
		} else {
			Vertex v = new Vertex("v" + actualVertexNumber++, location, this);
			vertices.add(v);
			changed();
			return v;
		}

	}

	/**
	 * Method for removing a vertex from vertices list and adjacencyList in the
	 * graph.
	 * 
	 * It also removes edges to or from that vertex
	 * 
	 * @param v
	 *            Vertex which should be removed
	 */
	public void removeVertex(Vertex v) {
		vertices.remove(v);
		HashSet<Edge> toRemove = new HashSet<Edge>();
		for (Edge e : edges) {
			if (e.meets(v)) {
				toRemove.add(e);
				adjacencyList.removeEdge(e);
			}
		}
		edges.removeAll(toRemove);
		changed();
	}

	/**
	 * Method for removing an edge from the edges list the graph.
	 * 
	 * @param e
	 *            Edge, which should be removed
	 */
	public void removeEdge(Edge e) {
		edges.remove(e);
		adjacencyList.removeEdge(e);
		changed();
	}

	/**
	 * Method for adding an edge to the edges list the graph.
	 * 
	 * @param from
	 *            Vertex of the starting point of the edge
	 * @param to
	 *            Vertex of the ending point of the edge
	 * @return Edge, which is added to the vertices list
	 */
	public Edge addEdge(Vertex from, Vertex to) {
		Edge e = new Edge(from, to, this);
		edges.add(e);
		adjacencyList.addEdge(e);
		changed();
		return e;
	}

	// /**
	// * Method for finding components in a graph
	// *
	// * It calls the same called method in the {@link Algorithm} class
	// */
	// public void findComponents() {
	// resetNonPersistentProps();
	// Algorithm.findComponents(this);
	// changed();
	// }

	/**
	 * Method for the algorithm of Prim.
	 * 
	 * It catches input errors, counts components and then calls the same called
	 * method in the {@link Algorithm} class
	 * 
	 * @param frame
	 *            parent frame
	 * @param view
	 *            the view object
	 * 
	 * @return UserInfo object containing userLog and map of mst-edges and non
	 *         mst-edges
	 */
	private UserInfo prim(GFrame frame, View view) {

		Vertex markedVertex = null;
		UserInfo userInfo = null;

		List<Vertex> verticesList = new ArrayList<Vertex>(this.getVertices());

		Object[] verticesArray = verticesList.toArray();

		markedVertex = (Vertex) JOptionPane.showInputDialog(frame,
				"Wählen Sie einen Startknoten:", "Information",
				JOptionPane.QUESTION_MESSAGE, null, verticesArray,
				verticesArray[0]);

		// if aborted
		if (markedVertex == null) {
			resetNonPersistentProps();
			view.update(null, null);

			// else marked Vertex was set through inputDialog
		} else {
			userInfo = Algorithm.prim(this, markedVertex);
		}

		return userInfo;

	}

	/**
	 * Method for counting components on the graph using the findComponents
	 * Algorithm.
	 * 
	 * @return integer value of the components in the graph
	 */
	private int countComponents() {
		int components = 0;
		int i = -1;

		while (i != 0 || i != -1) {
			i = Algorithm.findComponents(this);
			if (i == 0) {
				break;
			}
			components++;
		}

		return components;
	}

	/**
	 * 
	 * 
	 * @param frame
	 *            the parent frame in which the panel is included
	 * @param view
	 *            the drawing screen
	 * @param prim
	 *            boolean flag true - for prim algorithm, false for kruskal
	 * @throws PreConditionsException
	 */
	public void mst(GFrame frame, View view, boolean prim)
			throws PreConditionsException {
		resetNonPersistentProps();

		int vertices = this.getVertices().size();
		int components = 0;

		if (vertices >= 2) {

			components = countComponents();

			if (components == 1) {

				UserInfo.userFeedback(frame,
						"alle Knoten sind miteinander verbunden");

				UserInfo userInfo;
				// prim
				if (prim) {
					userInfo = prim(frame, view);
				} else {// or kruskal
					userInfo = kruskal();
				}

				if (userInfo != null) {
					setMarked(frame, view, userInfo.getEdges());
					textArea.setText(userInfo.getUserLog().toString());

					JOptionPane.showMessageDialog(frame, scrollPane,
							"Ergebnis der Berechnung",
							JOptionPane.INFORMATION_MESSAGE);
				}

				changed();

			} else {
				resetNonPersistentProps();
				view.update(null, null);
				throw new PreConditionsException(
						"alle Knoten müssen miteinander verbunden sein");

			}

		} else if (vertices == 1) {
			throw new PreConditionsException(
					"Die MST-Berechnung benötigt mindestens 2 Knoten");
		} else {
			throw new PreConditionsException("Es existieren keine Knoten");
		}

	}

	/**
	 * Wrapper method for executing the kruskal mst algorithm.
	 * 
	 * @return UserInfo object containing userLog and map of mst-edges and non
	 *         mst-edges
	 */
	private UserInfo kruskal() {
		return Algorithm.kruskal(this);
	}

	/**
	 * Sets color of edges in a map of edges and integer values where the
	 * integer value is the new color value.
	 * 
	 * @param frame
	 *            the parent frame in which the panel is included
	 * @param view
	 *            the drawing screen
	 * @param toBeMarked
	 *            map including edges and belonging integer values
	 * 
	 */
	private void setMarked(GFrame frame, View view,
			Map<Edge, Integer> toBeMarked) {

		for (Map.Entry<Edge, Integer> entry : toBeMarked.entrySet()) {
			UserInfo.userFeedback(frame, entry.getKey().toString());

			entry.getKey().setColour(entry.getValue());
			changed();
			update(entry.getKey());
		}

	}

	/**
	 * Method for resetting non persistent properties.
	 */
	public void resetNonPersistentProps() {
		for (Vertex v : vertices)
			v.resetNonPersistentProps();
		for (Edge e : edges)
			e.resetNonPersistentProps();
		changed();
	}

	/**
	 * Method for notifying the observers becaus of data change.
	 */
	protected void changed() {
		setChanged();
		notifyObservers();
	}

	/**
	 * Updates one Edge in the Hashmap of all Edges by deleting and adding again
	 * notice: contains only checks ids, which are equal.
	 * 
	 * @param edge
	 */
	public void update(Edge edge) {
		edges.remove(edge);
		edges.add(edge);
	}

	// getters & setters

	public void setChanged(boolean changed) {
		this.changed = changed;
	}

	public boolean isChanged() {
		return changed;
	}

	protected void setChanged() {
		super.setChanged();
		changed = true;
	}

	public HashSet<Vertex> getVertices() {
		return vertices;
	}

	public HashSet<Edge> getEdges() {
		return edges;
	}

	public HashSet<Vertex> getNeighboursOf(Vertex v) {
		return adjacencyList.getNeighboursOf(v);
	}

}
