package gModel;

import gView.GFrame;
import gView.Toast;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.Timer;

/**
 * Data Transfer Object for the report message to the user.
 * 
 * Also provides a static method to create userFeedback JDialogs.
 * 
 * @version 1.0 04.12.2014
 * @author Hinderlich
 * @author Ludwig
 *
 */
public class UserInfo {

	/**
	 * 
	 */
	static final int COLOR_GREEN = 3;

	/**
	 * Buffer for the userlog message.
	 */
	private StringBuffer userLog;
	/**
	 * Map of all edges marked by its value as mst edge (value 3) and no-mst
	 * edge (value 1).
	 */
	private Map<Edge, Integer> edges;
	/**
	 * List for the mstEdges.
	 */
	private List<Edge> mstEdges;

	/**
	 * The weight of the full minimal spanning tree, set to 0.
	 */
	private int mstWeight = 0;

	/**
	 * Constructor for the UserInfo class.
	 * 
	 * @param userLog
	 *            buffer of user log message
	 * @param edges
	 *            map containing all mst-edges and non-mst edges separated by
	 *            its value of 1-nonmst and 3-mst
	 */
	public UserInfo(StringBuffer userLog, Map<Edge, Integer> edges) {
		this.userLog = userLog;
		this.edges = edges;
		this.mstEdges = new ArrayList<Edge>();

		for (Map.Entry<Edge, Integer> element : this.edges.entrySet()) {
			if (element.getValue() == COLOR_GREEN) {
				this.mstWeight += element.getKey().getWeight();
				mstEdges.add(element.getKey());
			}
		}

		this.userLog.append("\n\nDer MST beinhaltet somit folgende Kanten:\n");

		formatMstEdges();

		this.userLog.append("und hat ein Gesamtgewicht von " + this.mstWeight);
	}

	/**
	 * Helper method for formatting user log output.
	 */
	private void formatMstEdges() {
		for (Edge edge : mstEdges) {
			this.userLog.append(edge + "\n");
		}
	}

	/**
	 * Method to show user feedback as modal dialog like android Toast messages,
	 * e.g. after catching an exception.
	 * 
	 * 
	 * @param frame
	 *            parent frame
	 * @param msg
	 *            message to the user
	 */
	public static void userFeedback(GFrame frame, String msg) {

		boolean modal = true;

		final Toast toast = new Toast(frame, modal, msg);

		Timer timer = new Timer(Toast.LENGTH_SHORT, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				toast.setVisible(false);
				toast.dispose();
			}
		});

		timer.setRepeats(false);
		timer.start();

		toast.setVisible(true);

	}

	// getter & setter

	public StringBuffer getUserLog() {
		return userLog;
	}

	public void setUserLog(StringBuffer userLog) {
		this.userLog = userLog;
	}

	public Map<Edge, Integer> getEdges() {
		return edges;
	}

	public void setEdges(Map<Edge, Integer> mstEdges) {
		this.edges = mstEdges;
	}

}
