package gModel;

import java.awt.Point;
import java.io.Serializable;
import java.util.HashSet;

/**
 * Vertex model class.
 * 
 * implements Serializable for save and restore in a graph
 * 
 * @version 1.0 04.12.2014
 * @author Hinderlich
 * @author Ludwig
 *
 */
public class Vertex implements Serializable {

	/**
	 * Default UID
	 */
	private static final long serialVersionUID = 1L;
	String name;
	Point location;
	boolean marked;
	int colour;
	Graph g;

	/**
	 * Constructor of a vertex.
	 * 
	 * @param name
	 *            String value of the vertex
	 * @param location
	 *            Point of the x and y coordinates
	 * @param g
	 *            Graph on which the Vertex should be painted
	 */
	public Vertex(String name, Point location, Graph g) {
		super();
		this.name = name;
		this.location = location;
		this.g = g;
		this.marked = false;
		this.colour = 0;
	}

	/**
	 * Method for resetting the marked flag and colour to default.
	 */
	public void resetNonPersistentProps() {
		marked = false;
		colour = 0;
	}

	// getter & setter

	public int getColour() {
		return colour;
	}

	public void setColour(int colour) {
		this.colour = colour;
	}

	public void incColour() {
		this.colour = colour + 1;
	}

	public boolean isMarked() {
		return marked;
	}

	public void setMarked(boolean marked) {
		this.marked = marked;
		g.changed();
	}

	public String getName() {
		return name;
	}

	public String toString() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Point getLocation() {
		return location;
	}

	/**
	 * Setter for the Location of the vertex, also updates the graph.
	 * 
	 * @param location
	 *            Point with x and y coordinates of new location
	 */
	public void setLocation(Point location) {
		this.location = location;
		g.changed();
	}

	public HashSet<Vertex> getNeighbours() {
		return g.getNeighboursOf(this);
	}

}
