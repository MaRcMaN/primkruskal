package gModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/**
 * Service class for Algorithm implementation.
 * 
 * @version 1.0 04.12.2014
 * @author Hinderlich
 * @author Ludwig
 */
public class Algorithm {

	/**
	 * Static value of colour set to 0.
	 */
	private static int colour = 0;
	/**
	 * Value of red colour set to 1.
	 */
	private static final int COLOR_RED = 1;
	/**
	 * Value of green colour set to 3.
	 */
	private static final int COLOR_GREEN = 3;

	/**
	 * Algorithm for finding components in a graph.
	 * 
	 * @param g
	 *            the Graph
	 *
	 * @return integer flag no component(-1), one component(0), more
	 *         components(2)
	 * 
	 */
	public static int findComponents(Graph g) {
		colour = colour + 1;
		Stack<Vertex> agenda = new Stack<Vertex>();
		HashSet<Vertex> vertices = g.getVertices();
		if (vertices.isEmpty()) {
			return -1;
		}
		Iterator<Vertex> it = vertices.iterator();
		Vertex first = it.next();
		while (first.isMarked()) {
			if (!it.hasNext()) {
				return 0;
			}
			first = it.next();
		}
		agenda.push(first);
		while (!agenda.isEmpty()) {
			Vertex actual = agenda.pop();
			if (!actual.isMarked()) {
				actual.setMarked(true);
				actual.setColour(colour);
				for (Vertex v : actual.getNeighbours()) {
					if (!agenda.contains(v)) {
						agenda.push(v);
					}
				}
			}
		}

		return 2;

	}

	/**
	 * Prim's Algorithm for computing a minimum spanning tree in an
	 * edge-weighted graph.
	 * 
	 * @param g
	 *            A weighted connected Graph Output: An MST for G
	 * @param startVertex
	 *            the startVertex for the algorithm
	 * 
	 * @return UserInfo object containing userLog and map of mst-edges and non
	 *         mst-edges
	 * 
	 */
	public static UserInfo prim(Graph g, Vertex startVertex) {

		/**
		 * user log message for algorithm feedback
		 */
		StringBuffer userLog = new StringBuffer();

		/**
		 * Set of all vertices in the graph.
		 */
		Set<Vertex> vertices = g.getVertices();

		/**
		 * number of all vertices in the graph
		 */
		int n = vertices.size();

		/**
		 * Set of all edges in the graph.
		 */
		Set<Edge> edges = g.getEdges();

		/**
		 * agenda list of vertices
		 */
		List<Vertex> U = new ArrayList<Vertex>();
		U.add(startVertex);

		/**
		 * map for the the mst and non mst edges marked by its values 1 (red) -
		 * non mst edge, 3(green) - mst edge
		 */
		Map<Edge, Integer> primEdges = new LinkedHashMap<Edge, Integer>();

		/**
		 * agenda list of edges
		 */
		List<Edge> A = new ArrayList<Edge>();

		/**
		 * Vertex pointing to the current vertex, starting with the marked
		 * startVertex
		 */
		Vertex currentVertex = startVertex;

		while (U.size() < n) {

			// choose a lightest edge
			for (Vertex v : U) {
				for (Vertex neighbour : v.getNeighbours()) {

					// for all neighbours check if edge is already handled
					for (Edge edge : edges) {

						// check if agenda or mstlist already include current
						// edge
						boolean isNotDuplicate = !A.contains(edge)
								&& !primEdges.containsKey(edge);

						// check both sides of edges from right to left and from
						// left to right
						boolean leftToRight = edge.getfrom().equals(v)
								&& edge.getTo().equals(neighbour)
								&& isNotDuplicate;

						boolean rightToLeft = edge.getfrom().equals(neighbour)
								&& edge.getTo().equals(v) && isNotDuplicate;

						if (leftToRight || rightToLeft) {
							A.add(edge);
						}

					}
				}
			}

			List<Edge> tmp = new ArrayList<Edge>(A);

			/*
			 * delete edges from A if their from- and to-vertices are already
			 * processed and included in U
			 */
			for (Edge edge : tmp) {
				Vertex from = edge.getfrom();
				Vertex to = edge.getTo();

				if (U.contains(from) && U.contains(to)) {
					A.remove(edge);
					primEdges.put(edge, COLOR_RED);
				}
			}

			// sort the edgesAgenda in ascending mode
			Collections.sort(A);

			Edge comparableEdge = A.get(0);

			// get vertex of shortest edge
			if (U.contains(comparableEdge.getfrom())) {
				currentVertex = comparableEdge.getTo();
			} else if (U.contains(comparableEdge.getTo())) {
				currentVertex = comparableEdge.getfrom();
			}

			// A = A ∪ { e } ;
			primEdges.put(A.get(0), COLOR_GREEN);
			A.remove(0);
			// U = U ∪ { v }
			U.add(currentVertex);

		}

		/*
		 * algorithm finished, now add vertices to the map which are not part of
		 * the minimal spanning tree and set their value to 1 (red)
		 */
		for (Edge edge : edges) {
			if (!primEdges.keySet().contains(edge)) {
				primEdges.put(edge, COLOR_RED);
			}
		}

		return new UserInfo(userLog, primEdges);

	}

	/**
	 * Algorithm for finding a minimal spanning tree of a graph G = (V, E, w)
	 * using the algorithm of Kruskal.
	 * 
	 * @param g
	 *            A weighted connected Graph Output: An MST for G
	 * 
	 * @return UserInfo object containing userLog and map of mst-edges and non
	 *         mst-edges
	 * 
	 */
	public static UserInfo kruskal(Graph g) {

		/**
		 * user log message for algorithm feedback
		 */
		StringBuffer userLog = new StringBuffer();

		/**
		 * set of all vertices of the graph.
		 */
		Set<Vertex> graphVertices = g.getVertices();

		/**
		 * List of all edges for sorting initialized with the set of all edges
		 * from the graph.
		 */
		List<Edge> graphVerticesList = new ArrayList<Edge>(g.getEdges());

		/**
		 * Empty map for the mst which will be returned at the end, contains mst
		 * edges with value 3 (green) and no-mst edges with value 1 (red)
		 */
		Map<Edge, Integer> separatedVertices = new LinkedHashMap<Edge, Integer>();

		userLog.append("Schritt 1:  Initialisiere A = {} \n\n");

		/**
		 * list which contains all subsets of one-member sets of all edges
		 */
		List<Set<Vertex>> superList = new ArrayList<Set<Vertex>>();

		for (Vertex vertex : graphVertices) {
			superList.add(makeSet(vertex));
		}
		userLog.append("Schritt 2:  Erstelle für jeden Knoten im Graph eine eigene Menge\n"
				+ superList.toString() + "\n\n");

		Collections.sort(graphVerticesList);
		userLog.append("Schritt 3:  Sortieren der Kanten nach Gewichtung (aufsteigend)\n"
				+ graphVerticesList + "\n\n");

		userLog.append("Schritt 4:  Prüfe für jede geordnete Kante ob Anfangs- und Endpunkt in der gleichen Menge liegen: \n\n");
		for (Edge edge : graphVerticesList) {

			userLog.append("Kante " + edge + ": ");

			Vertex from = edge.getfrom();
			Vertex to = edge.getTo();

			// find from in superset and get id
			Set<Vertex> fromSet = findSet(superList, from);

			// find to in superset and get id
			Set<Vertex> toSet = findSet(superList, to);

			if (fromSet != toSet) {
				userLog.append(edge.getfrom()
						+ " und "
						+ edge.getTo()
						+ " liegen nicht in der gleichen Menge, werden vereinigt ");
				separatedVertices.put(edge, 3);
				/* union */
				superList.remove(toSet);
				superList.remove(fromSet);

				superList.add(union(fromSet, toSet));
			} else {
				userLog.append("liegen in der gleichen Menge");
				separatedVertices.put(edge, 1);
			}

			userLog.append("\n");

		}

		return new UserInfo(userLog, separatedVertices);

	}

	/**
	 * Helper method for Kruskal's algorithm.
	 * 
	 * Create a new one-member set from one vertex
	 * 
	 * @param v
	 *            the vertex which will be converted to a one member set
	 * 
	 * @return one member set with the vertex in it
	 */
	private static Set<Vertex> makeSet(final Vertex v) {
		Set<Vertex> oneMemberSet = new HashSet<Vertex>();
		oneMemberSet.add(v);
		return oneMemberSet;
	};

	/**
	 * Helper method for Kruskal's algorithm.
	 *
	 * Creates a union set of two sets
	 * 
	 * @param first
	 *            the first subset
	 * @param second
	 *            the seconds subset
	 * @return union set of the two subsets
	 */
	private static Set<Vertex> union(final Set<Vertex> first,
			final Set<Vertex> second) {
		Set<Vertex> unionSet = new HashSet<Vertex>(first);
		unionSet.addAll(second);
		return unionSet;
	};

	/**
	 * /** Helper method for Kruskal's algorithm.
	 * 
	 * Looks for a the Set in which a specific vertex is included
	 * 
	 * @param superSet
	 *            overall set which contains all subsets of the graph
	 * @param vertex
	 *            vertex which should be found in the superset
	 * @return the Set in which the vertex is contained, or null if there is no
	 *         Set that contains the vertex
	 */
	private static Set<Vertex> findSet(final List<Set<Vertex>> superSet,
			final Vertex vertex) {

		Set<Vertex> found = null;

		for (Set<Vertex> currentSet : superSet) {
			if (currentSet.contains(vertex)) {
				found = currentSet;
				break;
			}
		}

		return found;
	};

}
