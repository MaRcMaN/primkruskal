package gView;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * Specific JDialog without Buttons for showing information to the user like the
 * Toast component in android.
 * 
 * @version 1.0 09.12.2014
 * @author Hinderlich
 * @author Ludwig
 *
 */
public class Toast extends JDialog {

	/**
	 * Unique serial id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * User message which is shows by the dialog.
	 */
	private String msg;
	/**
	 * Parent frame.
	 */
	private JFrame frame;
	/**
	 * Label ui component which includes the msg.
	 */
	private JLabel label;

	/**
	 * Java constant which is used for the duration of the shown dialog, in
	 * milliseconds, 5000ms --> 5s.
	 */
	public static final int LENGTH_LONG = 5000;
	/**
	 * 1000ms --> 1s.
	 */
	public static final int LENGTH_SHORT = 1500;

	/**
	 * Constructor of the Toast dialog.
	 * 
	 * @param frame
	 *            the parent frame in which the panel is included
	 * @param modal
	 *            flag which sets the dialog to modal or not
	 * @param msg
	 *            the message which should be displayed in the dialog
	 * 
	 */
	public Toast(final JFrame frame, final boolean modal, final String msg) {
		super(frame, modal);
		this.msg = msg;
		this.frame = frame;
		label = new JLabel();

		initComponents();
	}

	/**
	 * Initialize method to set the layout, size and position of the dialog.
	 */
	private void initComponents() {
		setLayout(new FlowLayout(FlowLayout.CENTER));
		addComponentListener(new ComponentAdapter() {
			// Give the window an rounded rect shape.

			@Override
			public void componentResized(final ComponentEvent e) {
				setShape(new RoundRectangle2D.Double(0, 0, getWidth(),
						getHeight(), 30, 30));
			}
		});
		setUndecorated(true);
		setSize(400, 30);
		setLocationRelativeTo(frame);

		Point point = getLocation();
		point.y -= 200;

		setLocation(point);
		getContentPane().setBackground(Color.BLACK);

		label.setForeground(Color.WHITE);
		label.setText(msg);
		add(label);
	}

	/**
	 * Setter method to change the label text of the dialog.
	 * 
	 * @param changed
	 *            the changed value, the label text should be set to
	 */
	public final void setText(final String changed) {
		remove(label);
		label.setText(changed);
		add(label);
		update(getGraphics());
	}
}
