package gView;

import java.awt.Color;

public class Config {

	public static final Color VERTEXCOLOUR = Color.darkGray;
	public static final Color ACTIVEVERTEXCOLOUR = Color.RED;
	public static final Color EDGECOLOUR = Color.darkGray;
	public static final Color[] COLOURS = { Color.darkGray, Color.RED,
			Color.BLUE, Color.GREEN, Color.YELLOW };
	public static final int VERTEXRADIUS = 10;

	public static final String FILENAMESUFFIX = ".grf";

	public static final double EPSILON = 3.0;

	/**
	 * maximum value of edges, which can be created
	 */
	public static final int MAXIMAL_VERTICES = 20;

}
