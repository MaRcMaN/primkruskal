package gView;

import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

import gController.GraphPanel;
import gController.InfoPanel;
import gController.ModePanel;
import gModel.Graph;

import javax.swing.JPanel;

public class View extends JPanel implements Observer {

	private static final long serialVersionUID = 1L;
	ModePanel actionPanel;
	InfoPanel infoPanel;
	GraphPanel graphPanel;
	Graph graph;

	public View(Graph graph, GFrame frame) {
		this.graph = graph;
		graph.addObserver(this);
		setLayout(new BorderLayout());
		graphPanel = new GraphPanel(frame, graph, this);
		this.add(graphPanel, BorderLayout.CENTER);

		actionPanel = new ModePanel(this, graphPanel);
		this.add(actionPanel, BorderLayout.WEST);

		infoPanel = new InfoPanel(frame, this, graphPanel);
		this.add(infoPanel, BorderLayout.SOUTH);

	}

	@Override
	public void update(Observable arg0, Object arg1) {
		repaint();
	}

	public Graph getGraph() {
		return graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
		graphPanel.setGraph(graph);
		graph.addObserver(this);
		repaint();
	}

	public void clear() {
		graphPanel.delete();
	}

	public InfoPanel getInfoPanel() {
		return this.infoPanel;
	}

	public void setInfoPanel(InfoPanel infoPanel) {
		this.infoPanel = infoPanel;
	}

	public ModePanel getActionPanel() {
		return actionPanel;
	}

	public void setActionPanel(ModePanel actionPanel) {
		this.actionPanel = actionPanel;
	}

	public GraphPanel getGraphPanel() {
		return graphPanel;
	}

	public void setGraphPanel(GraphPanel graphPanel) {
		this.graphPanel = graphPanel;
	}
}
