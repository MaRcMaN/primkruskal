package gController;

import gException.EdgeCreateException;
import gException.MaximalVerticesException;
import gModel.Edge;
import gModel.Graph;
import gModel.UserInfo;
import gModel.Vector;
import gModel.Vertex;
import gView.Config;
import gView.GFrame;
import gView.View;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

/**
 * GraphPanel controller class.
 * 
 * @version 1.0 04.12.2014
 * @author Hinderlich
 * @author Ludwig
 *
 */
public class GraphPanel extends JPanel implements MouseListener,
		MouseMotionListener {

	private static final long serialVersionUID = 1L;

	private Graph graph;
	private View view;
	private GFrame frame;

	private Vertex dragged;
	private Vertex markedVertex;
	private Edge markedEdge;
	private Point movingTo;
	private int mode;

	public static final int MARK = 0;
	public static final int NEWVERTEX = 1;
	public static final int NEWEDGE = 2;

	/**
	 * Error message for equal edges.
	 */
	private static final String ERROR_EDGE_EQUAL = "Anfangs- und Endknoten dürfen nicht gleich sein";
	/**
	 * Error message for existing edge.
	 */
	private static final String ERROR_EDGE_EXISTS = "Die Kante existiert bereits im Graph";

	/**
	 * /** Contructor for the GraphPanel.
	 * 
	 * @param frame
	 *            the parent frame in which all panels are included
	 * @param graph
	 *            the view of the graph which contains alle vertices and edges
	 * @param view
	 *            the drawing screen
	 */
	public GraphPanel(final GFrame frame, final Graph graph, final View view) {
		super();
		addMouseListener(this);
		addMouseMotionListener(this);
		this.frame = frame;
		this.graph = graph;
		this.view = view;
		this.setBorder(new BevelBorder(BevelBorder.RAISED));
		init();

	}

	/**
	 * Initialisation method for GraphPanel sets marked vertex, edge and dragged
	 * to null.
	 */
	private void init() {
		markedVertex = null;
		markedEdge = null;
		dragged = null;
	}

	/**
	 * Draws all components to the screen beginning with the edges, then
	 * vertices and after all the mouse dragged line which is not a edge right
	 * now.
	 * 
	 * @param g
	 *            graphics object for drawing lines on the screen
	 */
	protected final void paintComponent(final Graphics g) {

		// use Graphics2D object to increase the linewidth
		Graphics2D g2 = (Graphics2D) g;
		g2.setStroke(new BasicStroke(3));

		// set Font to Arial and bigger size
		Font test = new Font("Arial", Font.PLAIN, 18);
		g.setFont(test);

		for (Edge e : graph.getEdges()) {
			paintEdge(e, g);
		}
		for (Vertex v : graph.getVertices()) {
			paintVertex(v, g);
		}
		if (dragged != null && movingTo != null) {
			g.drawLine(dragged.getLocation().x, dragged.getLocation().y,
					movingTo.x, movingTo.y);
		}

	}

	/**
	 * Draws vertex to the screen.
	 * 
	 * @param v
	 *            the vertex which is to be painted
	 * @param g
	 *            graphics object for drawing lines on the screen
	 */
	private void paintVertex(Vertex v, Graphics g) {
		int radius = Config.VERTEXRADIUS;
		Color color = g.getColor();
		int x = v.getLocation().x - radius;
		int y = v.getLocation().y - radius;
		Color newColor = (v.isMarked() ? Config.ACTIVEVERTEXCOLOUR
				: Config.VERTEXCOLOUR);
		if (v.getColour() > 0)
			newColor = Config.COLOURS[v.getColour() % Config.COLOURS.length];
		g.setColor(newColor);
		g.fillOval(x, y, 2 * radius, 2 * radius);
		g.setColor(Color.BLACK);

		int positionX = x + (radius / 2);
		int positionY = y + 5 + 3 * radius;
		g.drawString(v.getName(), positionX, positionY);
		g.setColor(color);
	}

	/**
	 * Paints an edge and edge's weight to GraphicPanel.
	 * 
	 * @param e
	 *            Edge which shall be painted
	 * @param g
	 *            Graphics object on which the edge is painted
	 */
	private void paintEdge(Edge e, Graphics g) {
		Color color = g.getColor();

		Color newColor = (e.isMarker() ? Config.ACTIVEVERTEXCOLOUR
				: Config.EDGECOLOUR);
		if (e.getColour() > 0) {
			newColor = Config.COLOURS[e.getColour() % Config.COLOURS.length];
		}

		g.setColor(newColor);
		Point from = e.getfrom().getLocation();
		Point to = e.getTo().getLocation();
		g.drawLine(from.x, from.y, to.x, to.y);
		g.setColor(color);

		int midx = (from.x + to.x) / 2;
		int midy = (from.y + to.y) / 2;

		g.drawString(String.valueOf(e.getWeight()), midx, midy);
	}

	/**
	 * Deletes marked vertex or edge from graph.
	 */
	public final void delete() {
		if (markedVertex != null) {
			graph.removeVertex(markedVertex);
		}
		if (markedEdge != null) {
			graph.removeEdge(markedEdge);
		}
	}

	/**
	 * Adds an edge to the graph.
	 * 
	 * @param from
	 *            starting vertex of the edge
	 * @param to
	 *            ending vertex of the edge
	 */
	private void addEdge(final Vertex from, final Vertex to) {
		graph.addEdge(from, to);
	}

	/**
	 * MouseClicked event for managing the creation and marking of edges and
	 * vertices.
	 * 
	 * @param e
	 *            Clicked Event
	 */
	@Override
	public final void mouseClicked(final MouseEvent e) {
		Point mousePoint = e.getPoint();

		// reset existing markers
		if (markedVertex != null) {
			markedVertex.setMarked(false);
			markedVertex = null;
		}
		if (markedEdge != null) {
			markedEdge.setMarker(false);
			markedEdge = null;
		}
		markedVertex = null;
		markedEdge = null;

		// set new marked vertex or edge
		if (mode == GraphPanel.MARK) {
			Vertex v = mouseMeetsVertex(mousePoint);
			markedVertex = v;
			InfoPanel infoPanel = view.getInfoPanel();

			infoPanel.setMode(InfoPanel.DISABLED);

			if (v != null) {
				v.setMarked(true);
				infoPanel.setMode(InfoPanel.VERTEX);
			} else {
				Edge edge = mouseMeetsEdge(mousePoint);
				markedEdge = edge;
				if (edge != null) {
					edge.setMarker(true);
					infoPanel.setMode(InfoPanel.EDGE);

				}
			}
		}
		if (mode == GraphPanel.NEWVERTEX) {
			try {
				graph.addVertex(mousePoint);
			} catch (MaximalVerticesException e1) {
				UserInfo.userFeedback(frame, e1.getMessage());
			}
		}
		view.repaint();
	}

	/**
	 * Needs to be implemented due to MouseListener Interface. No function right
	 * now.
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	/**
	 * Needs to be implemented due to MouseListener Interface. No function right
	 * now.
	 */
	@Override
	public void mouseExited(MouseEvent e) {
	}

	/**
	 * MouseListener which sets sets the dragged attribute result of
	 * mouseMeetsVertex method.
	 */
	@Override
	public final void mousePressed(final MouseEvent e) {
		dragged = mouseMeetsVertex(e.getPoint());
	}

	/**
	 * Method to check if a given point meets a vertex of the graph.
	 * 
	 * @param point
	 *            a point with x and y coordinates
	 * @return vertex if the point meets a vertex in the graph, null if not
	 */
	private Vertex mouseMeetsVertex(final Point point) {
		for (Vertex v : graph.getVertices()) {
			if (isInside(v, point)) {
				return v;
			}
		}

		return null;
	}

	private Edge mouseMeetsEdge(final Point point) {
		for (Edge e : graph.getEdges()) {
			if (meets(e, point)) {
				return e;
			}
		}
		return null;
	}

	private boolean isInside(Vertex v, Point p) {
		return (p.distance(v.getLocation()) < Config.VERTEXRADIUS);
	}

	private boolean meets(Edge e, Point p) {
		Point q = e.getfrom().getLocation();
		Point r = e.getTo().getLocation();
		Vector v = new Vector(q, p);
		Vector w = new Vector(q, r);
		return (v.scalarproduct(w) > 0 && Vector.dist(p, q, r) < Config.EPSILON);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Vertex to = this.mouseMeetsVertex(e.getPoint());
		if (mode == GraphPanel.NEWEDGE && dragged != null && to != null) {

			try {
				checkEdgeExists(to);
				checkEqual(to);
				addEdge(dragged, to);

			} catch (EdgeCreateException e1) {
				UserInfo.userFeedback(frame, e1.getMessage());
			}

		}
		dragged = null;
		movingTo = null;

		view.repaint();

	}

	/**
	 * Method which checks if the edge is already given in the graph.
	 * 
	 * @param to
	 *            end vertex of the edge
	 * @throws EdgeCreateException
	 *             if edge is already given
	 */
	private void checkEdgeExists(Vertex to) throws EdgeCreateException {
		// check if edge is not already set between the given vertices
		for (Edge edge : graph.getEdges()) {

			boolean leftToRight = edge.getfrom().equals(dragged)
					&& edge.getTo().equals(to);
			boolean rightToLeft = edge.getfrom().equals(to)
					&& edge.getTo().equals(dragged);

			if (leftToRight || rightToLeft) {
				throw new EdgeCreateException(ERROR_EDGE_EXISTS);
			}
		}

	}

	/**
	 * Method for checking if the dragged and the to vertex are equal.
	 * 
	 * @param to
	 *            end vertex of the edge
	 * @throws EdgeCreateException
	 *             if they are equal
	 */
	private void checkEqual(Vertex to) throws EdgeCreateException {
		if (dragged == to) {
			throw new EdgeCreateException(ERROR_EDGE_EQUAL);
		}

	}

	@Override
	public final void mouseDragged(final MouseEvent e) {
		Graphics g = this.getGraphics();
		if (dragged == null) {
			return;
		}
		if (mode == GraphPanel.MARK) {
			dragged.setLocation(e.getPoint());
		}
		if (mode == GraphPanel.NEWEDGE) {
			view.repaint();

			g.drawLine(dragged.getLocation().x, dragged.getLocation().y,
					e.getX(), e.getY());
			movingTo = e.getPoint();

		}

	}

	/**
	 * Needs to be implemented due to MouseListener Interface. No function right
	 * now.
	 */
	@Override
	public void mouseMoved(MouseEvent arg0) {
	}

	// Getter & Setter

	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public Edge getMarkedEdge() {
		return markedEdge;
	}

	public void setMarkedEdge(Edge markedEdge) {
		this.markedEdge = markedEdge;
	}

	public Vertex getMarkedVertex() {
		return markedVertex;
	}

	public void setMarkedVertex(Vertex markedVertex) {
		this.markedVertex = markedVertex;
	}

	public Graph getGraph() {
		return this.graph;
	}

	public void setGraph(Graph graph) {
		this.graph = graph;
		init();
	}

	public void setMode(int mode) {
		this.mode = mode;
	}

}
