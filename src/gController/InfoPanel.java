package gController;

import gException.RenameExeption;
import gModel.Edge;
import gModel.UserInfo;
import gModel.Vertex;
import gView.GFrame;
import gView.JLimitedTextField;
import gView.View;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;

/**
 * InfoPanel Controller class.
 * 
 * @version 1.0 04.12.2014
 * @author Hinderlich
 * @author Ludwig
 *
 */
public class InfoPanel extends JPanel implements ActionListener {

	/**
	 * Default serialUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Switch value for info mode 0 - disabled.
	 */
	public static final int DISABLED = 0;
	/**
	 * Switch value for info mode 1 - vertex.
	 */
	public static final int VERTEX = 1;
	/**
	 * Switch value for info mode 2 - edge.
	 */
	public static final int EDGE = 2;

	/**
	 * Default info message if InfoPanel is disabled.
	 */
	public static final String INFO_DISABLED = "Keine Auswahl getroffen.";
	/**
	 * Info message if vertex is marked.
	 */
	public static final String INFO_VERTEX = "Knotenname: ";
	/**
	 * Info message if edge is marked.
	 */
	public static final String INFO_EDGE = "Gewichtung festlegen: ";

	/**
	 * Value for accept buttons.
	 */
	public static final String BTN_ACCEPT = "Bestätigen";
	/**
	 * Error message if name is not changed.
	 */
	public static final String ERROR_EDGE_EQUAL = "Es wurden keine Änderungen am Namen vorgenommen";
	/**
	 * Error message if name should be changed to an existing vertex name.
	 */
	public static final String ERROR_EDGE_EXISTS = "Es existiert bereits eine Kante mit diesem Namen.";
	/**
	 * Error message if name should be changed to an empty value.
	 */
	public static final String ERROR_EDGE_INPUT_EMPTY = "Ein leerer Wert für den Kantennamen ist nicht erlaubt";
	/**
	 * Error message if name should be changed to a non alphanumeric value.
	 */
	public static final String ERROR_EDGE_INPUT = "Die Kante kann nur Alphanumerische Werte enthalten.";

	/**
	 * The current infomode set to 0.
	 */
	private int currentInfoMode = DISABLED;

	/**
	 * Showing a mode-specific message for the renaming of an edge, vertex or
	 * the default message.
	 */
	private JLabel label;
	/**
	 * Spinner for changing the weight of an edge.
	 */
	private JSpinner spinner;
	/**
	 * Accept button for the renaming of an vertex.
	 */
	private JButton vertexButton;
	/**
	 * Accept button for the renaming of an edge.
	 */
	private JButton edgeButton;
	/**
	 * Textfield for the renaming of an vertex, which is limited by specific
	 * characters.
	 */
	private JLimitedTextField textField;

	/**
	 * 
	 */
	private View view;
	/**
	 * GraphPanel controller.
	 */
	private GraphPanel graphPanel;
	/**
	 * Parent frame.
	 */
	private GFrame frame;

	/**
	 * Constructor for the InfoPanel.
	 * 
	 * @param frame
	 *            the parent frame in which the panel is included
	 * @param view
	 *            the drawing screen
	 * @param graphPanel
	 *            the controller of the graph
	 */
	public InfoPanel(final GFrame frame, final View view,
			final GraphPanel graphPanel) {
		super();

		this.frame = frame;
		this.view = view;
		this.graphPanel = graphPanel;

		label = new JLabel(INFO_DISABLED);
		textField = new JLimitedTextField(3);
		textField.setColumns(3);
		textField.setActionCommand("PressedReturn");
		textField.addActionListener(this);

		// SpinnerNumberModel(value,min,max,step)
		spinner = new JSpinner(new SpinnerNumberModel(50, 1, 100, 1));

		edgeButton = new JButton(BTN_ACCEPT);
		edgeButton.addActionListener(this);

		vertexButton = new JButton(BTN_ACCEPT);
		vertexButton.addActionListener(this);

		setLayout(new FlowLayout());
		this.setBorder(new TitledBorder("Info Panel"));
		add(label);
	}

	/**
	 * Removes all added components from InfoPanel.
	 */
	public final void clean() {
		remove(spinner);
		remove(vertexButton);
		remove(edgeButton);
		remove(textField);
	}

	/**
	 * Sets the labelvalue and adds specific ui-components depending on the
	 * three mode values which are set if nothing, a vertex or an edge is marked
	 * in the graph.
	 * 
	 * @param mode
	 *            integer value INFO_DISABLED - 0 , INFO_VERTEX - 1 and
	 *            INFO_EDGE - 2
	 */
	public final void setMode(final int mode) {

		String description = "";

		clean();

		currentInfoMode = mode;

		switch (currentInfoMode) {
		case DISABLED:
			description = INFO_DISABLED;
			break;
		case VERTEX:
			description = INFO_VERTEX;
			Vertex markedVertex = view.getGraphPanel().getMarkedVertex();
			textField.setText(markedVertex.getName());
			add(textField);
			add(vertexButton);

			break;
		case EDGE:
			description = INFO_EDGE;
			Edge edge = view.getGraphPanel().getMarkedEdge();
			spinner.setValue(edge.getWeight());
			add(spinner);
			add(edgeButton);
		default:
			break;
		}

		label.setText(description);

	}

	/**
	 * Action Listener of the edge and vertex button.
	 */
	@Override
	public final void actionPerformed(final ActionEvent e) {

		if (e.getActionCommand() == "PressedReturn") {
			try {
				changeVertexValue();
			} catch (RenameExeption e1) {
				UserInfo.userFeedback(frame, e1.getMessage());
			}
		}

		if (e.getSource() == edgeButton) {

			Edge edge = view.getGraphPanel().getMarkedEdge();
			int weight = Integer.parseInt(spinner.getValue().toString());
			edge.setWeight(weight);
			view.getGraph().update(edge);

		}

		if (e.getSource() == vertexButton) {

			try {
				changeVertexValue();
			} catch (RenameExeption e1) {
				UserInfo.userFeedback(frame, e1.getMessage());
			}
		}

	}

	/**
	 * Method for changing a vertex value.
	 * 
	 * @throws RenameExeption
	 *             if current and rename values are: equal, not alphanumeric,
	 *             already given by another vertex or empty
	 */
	private void changeVertexValue() throws RenameExeption {
		String input = textField.getText();
		boolean valid = textField.validateInput(input, "^[a-zA-Z0-9]+$");

		Vertex markedVertex = graphPanel.getMarkedVertex();

		HashSet<Vertex> graphVertices = graphPanel.getGraph().getVertices();

		boolean uniqueInput = checkUnique(graphVertices, input);

		if (input.equals(markedVertex.getName())) {
			throw new RenameExeption(ERROR_EDGE_EQUAL);
		}

		if (valid) {

			if (uniqueInput) {
				textField.setText(input);
				markedVertex.setName(input);
			} else {
				// reset to old value
				textField.setText(markedVertex.getName());
				throw new RenameExeption(ERROR_EDGE_EXISTS);

			}

			view.update(null, null);

		} else {
			// reset to old value
			textField.setText(markedVertex.getName());

			if (input.isEmpty()) {
				throw new RenameExeption(ERROR_EDGE_INPUT_EMPTY);
			} else {
				throw new RenameExeption(ERROR_EDGE_INPUT);
			}

		}
	}

	/**
	 * Method for checking the uniqueness of a vertices name when renaming.
	 * 
	 * @param vertices
	 *            set of all vertices
	 * @param name
	 *            the name, the marked vertex should be changed to
	 * @return true if name is unique, false if not
	 */
	public final boolean checkUnique(final HashSet<Vertex> vertices,
			final String name) {

		boolean unique = true;

		for (Vertex vertex : vertices) {
			if (vertex.getName().equals(name)) {
				unique = false;
				break;
			}
		}
		return unique;
	}

}